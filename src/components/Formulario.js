import React, { Fragment, useState } from 'react';

// * DOCUMENTATION
import PropTypes from 'prop-types';

//  * EXTERNAL PACKAGE
import { v4 as uuidv4 } from 'uuid';

const Formulario = ( {crearCita} ) => {



        // STATE     
        const [cita, actualizarCita] = useState({
                mascota: '',
                propietario: '',
                fecha: '',
                hora: '',
                sintomas: ''
        })

        const [ error, actualizarError ] = useState(false);

        // EXEUTES WHEN FILLING AN INPUT
        const actualizarState = e => {
                actualizarCita({
                        // * DESTRUCTUING STATE
                        ...cita,
                        [e.target.name] : e.target.value
                })
        }

        // DESTRUCTURING PROPERTIES
        const { mascota, propietario, fecha, hora, sintomas } = cita;

        // WHEN THE USER PRESS "ADD APPOINTMENT"
        const submitCita = (e) => {

                e.preventDefault();
                
                // * VALIDATE FORM
                if( mascota.trim() === '' ||
                    propietario.trim() === '' ||
                    fecha.trim() === '' ||
                    hora.trim() === '' ||
                    sintomas.trim() === ''
                    ){
                        actualizarError(true);
                        return;
                }

                actualizarError(false);

                // * ASSING ID
                cita.id = uuidv4();
                console.log(cita);


                // * CREATE APPOINTMENT
                crearCita(cita);

                // * RE START THE FORM
                actualizarCita({
                        mascota: '',
                        propietario: '',
                        fecha: '',
                        hora: '',
                        sintomas: ''
                })

        }

    return (
        <Fragment>
            <h2>Crear Cita</h2>

            { error 
                ? <p className="alerta-error">Todos los campos son obligatorios.</p>
                : null }
            <form
                onSubmit={submitCita}
            >

                <label>Nombre Mascota</label>
                <input 
                        type="text"
                        name="mascota"
                        className="u-full-width"
                        placeholder="Nombre Mascota"
                        onChange={actualizarState}
                        value={mascota}
                />

                <label>Nombre Dueño</label>
                <input
                        type="text"
                        name="propietario"
                        className="u-full-width"
                        placeholder="Nombre Dueño de la Mascota"
                        onChange={actualizarState}
                        value={propietario}
                />

                <label>Fecha</label>
                <input
                        type="date"
                        name="fecha"
                        className="u-full-width"
                        onChange={actualizarState}
                        value={fecha}
                />

                <label>Hora</label>
                <input
                        type="time"
                        name="hora"
                        className="u-full-width"
                        onChange={actualizarState}
                        value={hora}
                />

                <label>S&iacute;ntomas</label>
                <textarea
                        name="sintomas"
                        className="u-full-width"
                        onChange={actualizarState}
                        value={sintomas}
                >

                </textarea>

                <button
                        type="submit"
                        className="u-full-width button-primary"
                >Agregar cita</button>

            </form>
        </Fragment>
     );
}

Formulario.propTypes = {

        crearCita: PropTypes.func.isRequired

}

export default Formulario;