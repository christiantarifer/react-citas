import React, { Fragment, useState, useEffect } from 'react';

//  * COMPONENTS
import Cita from './components/Cita';
import Formulario from './components/Formulario';


function App() {

  // APPOINTEMENTS ON LOCAL STORAGE
  let citasIniciales =  JSON.parse( localStorage.getItem('citas') );
  
  if( !citasIniciales ) {

    citasIniciales = [];

  }

  // APPOINTMENT ARRAY
  const [ citas, guardarCitas] = useState([]);

  // USE EFFECT TO SOME OPERATION WHEN THE STATE CHANGE -> SIMILAR TO DOCUMENT READY - DOM CONTENT LOADED
  useEffect( () => {

    // APPOINTEMENTS ON LOCAL STORAGE
    let citasIniciales =  JSON.parse( localStorage.getItem('citas') );

    if ( citasIniciales ) {

      localStorage.setItem( 'citas', JSON.stringify(citas) );

    } else {

      localStorage.setItem( 'citas', JSON.stringify([]) );

    }

  }, [ citas ]);

  //  FUNCTION THAT TAKES ALL CURRENT APPOINTMENTS AND THE NEW ONE
  const crearCita = cita => {
    guardarCitas([
      ...citas,
      cita
    ])
  }

  // * FUNCTION THAT DELETES AN APPOINTMENT BY ITS ID
  const eliminarCita = id => {

    const nuevasCitas = citas.filter( cita => cita.id !== id);

    guardarCitas(nuevasCitas);

  }

  // * CONDITIONAL MESSAGE
  const titulo = citas.length === 0 ? 'No hay citas' : 'Administra tus Citas';

  return (
    <Fragment>

      <h1>Administrador de Pacientes</h1>

      <div className="container">

        <div className="row">

          <div className="one-half column">
            <Formulario crearCita={crearCita} />
          </div>

          <div className="one-half column">
            <h2> { titulo } </h2>
            {citas.map( cita => (
              <Cita 
                  key={cita.id}
                  cita={cita}
                  eliminarCita={eliminarCita}
              />
            ) )}
          </div>
        </div>

      </div>

    </Fragment>
    

  );
}

export default App;
